require 'spec_helper'

describe 'Descuento' do

  it 'Hoy es el dia del descuento y este se aplica' do
    proveedor_dia = double('ProveedorDia')
    allow(proveedor_dia).to receive(:obtener_dia).and_return(Date.parse('monday').wday)

    descuento = Descuento.new(10, Date.parse('monday').wday, proveedor_dia)
    producto = Producto.new('un_producto', 100)

    expect(descuento.aplicar_a(producto)).to eq(90)
  end

  it 'Hoy no es el dia del descuento y este no se aplica' do
    proveedor_dia = double('ProveedorDia')
    allow(proveedor_dia).to receive(:obtener_dia).and_return(Date.parse('tuesday').wday)

    descuento = Descuento.new(10, Date.parse('monday').wday, proveedor_dia)
    producto = Producto.new('un_producto', 100)

    expect(descuento.aplicar_a(producto)).to eq(100)
  end


end
