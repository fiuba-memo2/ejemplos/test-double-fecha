Dado('que el dia del descuento es {string} y es de {int}%') do |dia_descuento, porcentaje_descuento|
  @dia_descuento = dia_descuento
  @porcentaje_descuento = porcentaje_descuento
end

Dado('hoy es {string}') do |hoy|
  @hoy = hoy
end

Cuando('el cliente compra un producto de {int} pesos') do |precio|
  @precio = precio
end

Entonces('el cliente debe pagar {int} pesos') do |precio_final|
  comando = "HOY=#{@hoy} ruby app.rb producto #{@precio} #{@dia_descuento} #{@porcentaje_descuento}"
  resultado = `#{comando}`
  expect(resultado.strip).to eq(precio_final.to_s)
end
