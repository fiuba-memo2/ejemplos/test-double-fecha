# language: es

  Característica: Descuento por dia

    Escenario: Hoy es el dia del descuento y este se aplica
      Dado que el dia del descuento es "Lunes" y es de 5%
      Y hoy es "Lunes"
      Cuando el cliente compra un producto de 500 pesos
      Entonces el cliente debe pagar 475 pesos

    Escenario: Hoy no es el dia del descuento y este no se aplica
      Dado que el dia del descuento es "Lunes" y es de 5%
      Y hoy es "Martes"
      Cuando el cliente compra un producto de 500 pesos
      Entonces el cliente debe pagar 500 pesos


