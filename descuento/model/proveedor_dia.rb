class ProveedorDia
  def obtener_dia
    Date.today.wday
  end
end

class ProveedorDiaDouble
  def initialize(dia)
    @dia = dia
  end

  def obtener_dia
    @dia
  end
end
