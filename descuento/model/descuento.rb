require 'date'

class Descuento
  attr_accessor :porcentaje_descuento, :dia_descuento

  def initialize(porcentaje_descuento, dia_descuento, proveedor_dia)
    @porcentaje_descuento = porcentaje_descuento
    @dia_descuento = dia_descuento
    @proveedor_dia = proveedor_dia
  end

  def aplicar_a(producto)
    if hoy_es_el_dia_del_descuento?
      producto.precio - (producto.precio * @porcentaje_descuento / 100).to_i
    else
      producto.precio
    end
  end

  private

  def hoy_es_el_dia_del_descuento?
    @proveedor_dia.obtener_dia == @dia_descuento
  end
end
