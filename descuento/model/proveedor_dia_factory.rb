require_relative 'proveedor_dia'

class ProveedorDiaFactory
  def self.get(dias)
    if ENV['HOY']
      ProveedorDiaDouble.new(dias[ENV['HOY']])
    else
      ProveedorDia.new
    end
  end
end
