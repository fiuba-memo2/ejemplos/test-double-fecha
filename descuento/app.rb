require_relative './model/producto'
require_relative './model/descuento'
require_relative './model/proveedor_dia_factory'

require 'date'
require 'rspec'

DIAS = {
  'Lunes' => Date.parse('monday').wday,
  'Martes' => Date.parse('tuesday').wday,
  'Miercoles' => Date.parse('wednesday').wday,
  'Jueves' => Date.parse('thursday').wday,
  'Viernes' => Date.parse('friday').wday,
  'Sabado' => Date.parse('saturday').wday,
  'Domingo' => Date.parse('sunday').wday
}.freeze

nombre_producto = ARGV[0]
precio_producto = ARGV[1].to_i
dia_descuento = DIAS[ARGV[2]]
porcentaje_descuento = ARGV[3].to_i
proveedor_dia = ProveedorDiaFactory.get(DIAS)

producto = Producto.new(nombre_producto, precio_producto)
descuento = Descuento.new(porcentaje_descuento, dia_descuento, proveedor_dia)
precio_final = descuento.aplicar_a(producto)

puts precio_final.to_s
